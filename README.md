# React courses, exercises and projects

## Summary

Welcome to the repository for React courses, exercises, and projects! Below are brief descriptions of the courses available:


### Mobile Application Development

In this course student will learn the basics of the mobile application developing with popular Cross-Platform Tools. The student can design, implement and publish mobile applications to mobile devices. Course contains lectures, exercises and research assignment.

The student understands the prospects of different mobile technologies for implementation of functional applications in mobile devices. The student is able to implement small-scale applications for various mobile platforms using various mobile technologies. The student is able to work as a project group member in a project concerning a research assignment.

The topics discussed are commonly and currently used cross-platform mobile technologies that can be used to implement applications for various mobile platforms. The students deepen their competence by selecting one technology for a mobile device and implementing a research assignment on the topic area in small groups.

<a href='./mobile-application-development/'>Code</a>

### Web User Interface Programming

In this course student will learn the basics of the application developing in a web environment. The student can design, implement and publish applications to web. Course contains lectures, exercises and research assignment.

This course mainly focuses on the framework(s) and libraries used for developing applications for web using JavaScript. 

<a href='./web-user-interface-programming/'>Code</a>






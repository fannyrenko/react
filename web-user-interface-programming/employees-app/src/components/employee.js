import React from 'react'

function Employee(props) {
  const { firstName, lastName, email, phone, title, department, image } = props.employee

  return (
    <div className='Employee'>
     
      <img src={image} className='employeeImg'></img>
      <div>
        <p className='Name'>{firstName} {lastName}</p>
        <p className='title'>{title}</p>
        <p className='department'>{department}</p>
        <p className='email'>{email}</p>
        <p className='phone'>{phone}</p>
      </div>
    </div>
  )
}

export default Employee
import React, { useState, useEffect } from 'react'
import axios from 'axios'
import Employee from './components/employee'
import './components/employee.css'

function App() {

  // Hook state
  const [employees, setEmployees] = useState([])

  useEffect(() => {
    axios
      .get('http://localhost:3001/employees')
      .then(response => {
        console.log('employees loaded')
        console.log(response.data)
        setEmployees(response.data)
      }). catch(err => {
        // error handling
        console.log(err)
      })
  }, []) // load once => []

  const employeeItems = employees.map((employee, index) => (
    <Employee key={index} employee={employee} />
  ))

  return (
    <div className="EmployeeContainer">
      {employeeItems}
    </div>
  )
}

export default App

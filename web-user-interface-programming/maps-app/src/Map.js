import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';
import markerIconPng from "leaflet/dist/images/marker-icon.png";
import {Icon} from 'leaflet';



function Map() {
  const [courses, setCourses] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    axios
      .get('http://localhost:3001/courses')
      .then(response => {
        console.log(response.data);
        setCourses(response.data);
        setLoading(false);
      })
      .catch(error => {
        console.error('Error fetching data:', error);
        setError(error);
        setLoading(false);
      });
  }, []);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error: {error.message}</p>;

  const position = [62, 26];
  const zoom = 7;

  const markers = courses.map((course, index) => (
    <Marker position={[course.lat, course.lng]} key={index} icon={new Icon({iconUrl: markerIconPng, iconSize: [25, 41], iconAnchor: [12, 41]})}>
      <Popup>
        <b>{course.course}</b>
        <br />
        {course.address}
        <br />
        {course.phone}
        <br />
        {course.email}
        <br />
        <a href={course.web} target="_blank" rel="noopener noreferrer">
          {course.web}
        </a>
        <br />
        <br />
        <i>{course.text}</i>
      </Popup>
    </Marker>
  ));

  return (
    <MapContainer style={{ height: "1000px", width: "100%" }} center={position} zoom={zoom} className="App">
      <TileLayer
        attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      {markers}
    </MapContainer>
  );
}

export default Map;

import React, { useState } from 'react';
import axios from 'axios';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Box from '@mui/material/Box';

function App() {
  
  const [cityname, setCityname] = useState("Jyväskylä");
  const [weather, setWeather] = useState(null);

  const API_KEY = process.env.REACT_APP_WEATHER_APIKEY; 
  const URL = 'https://api.openweathermap.org/data/2.5/weather?q=';
  const ICON_URL = 'http://openweathermap.org/img/wn/';

  const getWeather = () => {
    axios
      .get(URL + cityname + '&appid=' + API_KEY + '&units=metric')
      .then(response => {
        setWeather(response.data);
      })
      .catch(error => {
        console.error('Error fetching weather data:', error);
      });
  };

  return (
    <Container maxWidth="sm">
      <Box mt={5}>
        <Typography variant="h4" align="center" gutterBottom>
          React Weather Application
        </Typography>
        <Typography variant="body1" align="center" paragraph>
          This application will fetch a weather forecast from OpenWeather.
          Just type the city name and click Get Forecast button!
        </Typography>

        <form>
          <TextField
            label="Cityname"
            variant="outlined"
            fullWidth
            value={cityname}
            onChange={(e) => setCityname(e.target.value)}
          />
          
        </form>
        <div style={{ display: 'flex', justifyContent: 'center', marginTop: '10px' }}>
          <Button
            variant="contained"
            color="primary"
            size="large"
            onClick={() => getWeather()}
          >
            Get Forecast
          </Button>
      </div>

        <Box mt={4}>
          <Typography variant="h5" align="center" gutterBottom>
            Loaded weather forecast
          </Typography>
          {weather !== null &&
            <Box textAlign="center">
              <Typography variant="body1">
                City: {weather.name}<br />
                Main: {weather.weather[0].main}<br />
                Temp: {weather.main.temp} °C<br />
                Feels: {weather.main.feels_like} °C<br />
                Min-Max: {weather.main.temp_min} - {weather.main.temp_max} °C<br />
                <img
                  alt={cityname}
                  style={{ height: 100, width: 100 }}
                  src={ICON_URL + weather.weather[0].icon + '.png'}
                />
              </Typography>
            </Box>
          }

          {weather === null &&
            <Typography variant="body1" align="center">
              No data available
            </Typography>
          }
        </Box>
      </Box>
    </Container>
  );
}

export default App;

# Web User Interface Programming

## Couse discription

In this course student will learn the basics of the application developing in a web environment. The student can design, implement and publish applications to web. Course contains lectures, exercises and research assignment.

This course mainly focuses on the framework(s) and libraries used for developing applications for web using JavaScript. 

## Exercises

<a href='./ReactBasics/'>Exercise 01 : React Basics</a>

<a href='./todo-app/'>Exercise 02 : Todo</a>

<a href='./employees-app/'>Exercise 03 : Employees</a>

<a href='./movies-app/'>Exercise 04 : Load and parse JSON and show loaded movies data</a>

<a href='./weather-app/'>Exercise 05 : Weather Forecast</a>





import './App.css';
import React, { useState } from 'react';

function Banner() {
  return (
    <h1>Todo Example with React</h1>
  )
}

function ToDoFormAndList() {
  // Using the useState hook to create state variables for item text and todo items.
  const [itemText, setItemText ] = useState("");
  const [items, setItems] = useState([]);

  // Handling the form submission to add a new todo item.
  const handleSubmit = (event) => {
    event.preventDefault();
    // Creating a new todo item object with a unique ID and the entered text.
    setItems([...items, {id: Math.random(), text: itemText}])
    // Clearing the input field after adding the item.
    setItemText("")
  }

  // Handling the removal of a todo item based on its ID.
  const removeItem = (id) => {
  // Filtering out the item with the specified ID from the current list of items.
  const newItems = items.filter(item => item.id !== id);
  // Updating the state with the new list of items (item removed).
  setItems(newItems);
}


  return (
    <div>
      {/* Form for adding new todo items */}
      <form onSubmit={handleSubmit}>
      <input type='text' 
       value={itemText} 
       onChange={event => setItemText(event.target.value)} 
       placeholder="Write a new todo here" />
        <input type='submit' value='Add'/>
      </form>
      {/* List of todo items with the option to remove each item */}
      <ul>
        {items.map(item => (
      <li key={item.id}>
        {item.text+" "} <span onClick={() => removeItem(item.id)}> x </span>
      </li>
      ))}
    </ul>  
    </div>
  )  
}

function App() {
  return (
    <div>
        <Banner/>
        <ToDoFormAndList/>
    </div>
  );
}

export default App;

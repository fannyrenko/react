# Exercise 04 : Load and parse JSON and show loaded movies data

## Screenshots

### Fullscreen view
<img src='fullscreen.png' style='width:550px'>

### Mobile view
<img src='mobile.png' style='width:350px'>

### Open modal and show selected video
<img src='trailer.png' style='width:550px'>
import React, { useState, useEffect } from 'react'
import axios from 'axios'
import Modal from 'react-modal'
import YouTube from 'react-youtube'
import './App.css'

function App() {

  // component to list movies
  function MovieList() {

    // state to store the list of movies and loading status
    const [movies, setMovies] = useState([])
    const [loading, setLoading] = useState(true)

    // fetch the list of now playing movies when the component mounts
    useEffect(() => {
      axios
        .get('https://api.themoviedb.org/3/movie/now_playing?api_key=fa4c2789474544e4f94c0def73185a5d&append_to_response=videos')
        .then(response => {
          setMovies(response.data.results)
          setLoading(false)
        })
        .catch(error => {
          console.error('Error fetching now playing movies:', error)
          setLoading(false)
        })
    }, [])

    // state for modal to show YouTube trailer
    const [modalIsOpen, setModalIsOpen] = useState(false)
    const [modalVideoKey, setModalVideoKey] = useState('')

    // function to open the modal and set the video key
    const openModal = (videoKey) => {
      setModalVideoKey(videoKey)
      setModalIsOpen(true)
    }

    // function to close the modal
    const closeModal = () => {
      setModalVideoKey('')
      setModalIsOpen(false)
    }

    // if still loading, display a loading message
    if (loading) {
      return (
        <div style={{ flex: 1, padding: 20 }}>
          <p>Loading, please wait...</p>
        </div>
      )
    } else {
       // map through the list of movies and create MovieListItem components
      const movieItems = movies.map((movie, index) => (
        <MovieListItem key={index} movie={movie} openModal={openModal} />
      ))
  
      // render the list of movies and the modal
      return (
        <div className="movie-grid">
          {movieItems}
          <Modal
            isOpen={modalIsOpen}
            onRequestClose={closeModal}
            contentLabel="Trailer Modal"
            className="Modal"
            overlayClassName="Overlay"
          >
            {modalVideoKey ? (
              <YouTube videoId={modalVideoKey} opts={{ playerVars: { autoplay: 1 } }} />
            ) : (
              <div style={{ textAlign: 'center', paddingTop: '20px' }}>Loading...</div>
            )}
            <button onClick={closeModal}>Close Trailer</button>
          </Modal>
        </div>
      )
    }
  }

  // component to display details of a single movie
  function MovieListItem(props) {

    // state to store details of a movie and loading status
    const [movie, setMovie] = useState({})
    const [loading, setLoading] = useState(true)
    const [videoKey, setVideoKey] = useState('')

    // fetch details of a specific movie when the component mounts
    useEffect(() => {
      axios
        .get(`https://api.themoviedb.org/3/movie/${props.movie.id}?api_key=fa4c2789474544e4f94c0def73185a5d&append_to_response=videos`)
        .then(response => {
          setMovie(response.data)
          setLoading(false)
          const videos = response.data.videos?.results
          if (videos && videos.length > 0) {
            setVideoKey(videos[0].key)
          }
        })
        .catch(error => {
          console.error('Error fetching movie details:', error)
          setLoading(false)
        });
    }, [props.movie.id])

    // URL for movie poster image
    let IMAGEPATH = 'http://image.tmdb.org/t/p/w500'
    let imageurl = IMAGEPATH + movie.poster_path

    // generate a string with movie genres
    var genres = ""
    if (movie.genres !== undefined) {
      for (var i = 0; i < movie.genres.length; i++) {
        genres += movie.genres[i].name + " "
      }
    }

    // jsx for the "watch trailer" link
    var video = ""
    if (videoKey) {
      video = <span style={{ color: 'blue', cursor: 'pointer' }} onClick={() => props.openModal(videoKey)}>Watch Trailer</span>
    }

    // render details of the movie
    return (
      <div className="Movie">
        {!loading && (
          <>
            <img src={imageurl} alt={props.movie.original_title} />
            <p className="MovieTitle">{props.movie.original_title}</p>
            <p className="ReleaseDate">{props.movie.release_date}</p>
            <p className="MovieText">{props.movie.overview}</p>
            <span className="GenresText">Genres: {genres}</span><br />
            <span className="VideosText">Video: {video}</span>
          </>
        )}
      </div>
    )
  }

  // render the main App component
  return (
    <div className="App">
      <MovieList />
    </div>
  )
}

export default App

import React, { useState, useEffect } from 'react';
import axios from 'axios';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight
} from 'react-native';
import { useNavigation } from '@react-navigation/native';


const MovieListScreen = () => {
  const [movies, setMovies] = useState([]);
  const navigation = useNavigation();

  const itemPressed = (index) => {
    navigation.navigate('MovieDetails', { movie: movies[index] });
  }

  useEffect(() => {
    axios
      .get('https://api.themoviedb.org/3/movie/now_playing?api_key=fa4c2789474544e4f94c0def73185a5d&append_to_response=videos')
      .then(response => {
        console.log(response.data.results);
        setMovies(response.data.results);
      })
      .catch(error => {
        console.error('Error fetching movies:', error);
      });
  }, []);

  if (movies.length === 0) {
    return (
      <View style={{ flex: 1, padding: 20 }}>
        <Text>Loading, please wait...</Text>
      </View>
    );
  }

  let movieItems = movies.map(function(movie, index) {
    return (
      <TouchableHighlight
        onPress={() => itemPressed(index)}
        underlayColor="lightgray"
        key={index}
      >
        <MovieListItem movie={movie} navigation={navigation} />
      </TouchableHighlight>
    );
  });

  return (
    <ScrollView>
      {movieItems}
    </ScrollView>
  )
};


const styles = StyleSheet.create({
  movieItem: {
    margin: 5,
    flex: 1,
    flexDirection: 'row'
  },
  movieItemImage: {
    marginRight: 5
  },
  movieItemTitle: {
    fontWeight: 'bold',
  },
  movieItemText: {
    flexWrap: 'wrap'
  }
});

function MovieListItem(props) {
  let IMAGEPATH = 'http://image.tmdb.org/t/p/w500'
  let imageurl = IMAGEPATH + props.movie.poster_path;

  return (
    <View style={styles.movieItem}>
      <View style={styles.movieItemImage}>
        <Image source={{uri: imageurl}} style={{width: 99, height: 146}} />
      </View>
      <View style={{marginRight: 50}}>
        <Text style={styles.movieItemTitle}>{props.movie.title}</Text>
        <Text style={styles.movieItemText}>{props.movie.release_date}</Text>
        <Text numberOfLines={6} ellipsizeMode="tail" style={styles.movieItemText}>{props.movie.overview}</Text>
      </View> 
    </View>
  )
};

export default MovieListScreen;

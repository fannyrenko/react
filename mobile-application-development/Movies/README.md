# Exercise 04: Movies

In this exercise you will learn the basics of the React Native CLI development:

  - using and rendering build-in components
  - creating and rendering an own components
  - understand state and props
  - event handling
  - passing data between components
  - calling functions between components
  - using styles
  - navigating between screens
  - load and parse JSON data
  - touchable components

## Screenshots

<img src='screenshot_1.png' height=500>

<img src='screenshot_2.png' height=500>


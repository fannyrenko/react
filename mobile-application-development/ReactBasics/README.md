# Exercise 01 : React Basics
## Practice 1 - States and event handling

Create an application where user can increase/decrease value by pressing a Button components (change some other button colors to your solution). Value is visible in Text component.

<img src='./Practice1.png' height=400>

## Practice 2 - Event handling and component rendering

Create an application where a new random number will be shown inside a ScrollView, when a Button component is clicked.


<img src='./Practice2.png' height=400>

## Practice 3 - Component and props

Create own Movie component, which displays movie title, theatre and starting time. Use your own Movie component and display a few movie information.

<img src='./Practice3.png' height=400>
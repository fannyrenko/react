# Exercise 03: Todo / Expo

In this exercise you will learn the basics of the React Native development:
  - using and rendering build-in components
  - creating and rendering an own components
  - understand state and props
  - event handling
  - passing data between components
  - calling functions between components
  - using styles


## Screenshots

<img src='screenshot-2.png' height=450>

<img src='screenshot-1.png' height=450>

<img src='screenshot-3.png' height=450>
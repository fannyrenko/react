import { StatusBar } from 'expo-status-bar';
import { useState } from 'react'
import { ScrollView, StyleSheet, Text, TextInput, View, Button, Keyboard } from 'react-native';

function Banner() {
  return(
    <View style={styles.banner}>
      <Text style={styles.bannerText}>Todo Example with React Native</Text>
    </View>
  )
}

function ToDoList() {
  const [itemText, setItemText] = useState("")

  const[items, setItems] = useState([])

  const addToDoItem = () => {
    if (itemText !== '') {
      setItems([...items, {id: Math.random(), text:itemText}])
      setItemText('')
    }
    Keyboard.dismiss()
  }

  const removeItem = (id) => {
    const newItems = items.filter(item => item.id !== id)
    setItems(newItems)
  }


  return (
    <View>
      <View style={styles.addToDo}>
        <TextInput 
        style={styles.addToDoTextInput}
        value={itemText}
        onChangeText={ (text) => setItemText(text)}
        placeholder='Write a new todo here'
        />
        <Button 
        title='Add' 
        style={styles.addToDoButton}
        onPress={addToDoItem}
        />
      </View>
      <ScrollView styles={styles.list}>
        {items.map( (item, index) => (
          <View key={index} style={styles.listItem}>
            <Text style={styles.listItemText}>* {item.text}</Text>
            <Text style={styles.listItemDelete}
             onPress={() => removeItem(item.id)}>X</Text>
          </View>

        ))}
      </ScrollView>

    </View>
  )
}

export default function App() {
  return (
    <View style={styles.container}>
      <Banner/>
      <ToDoList/>
      <StatusBar style="auto" />
    </View>
  );
}

// styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 25,
    margin:5
  },
  banner: {
    backgroundColor: '#845ec2',
    justifyContent: 'center',
    marginBottom: 20,
    
  },
  bannerText: {
    color: 'white',
    fontSize: 20,
    textAlign: 'center',
    fontWeight: 'bold',
    paddingTop: 20,
    paddingBottom: 20 
  },
  addToDo: {
    flexDirection: 'row',
    marginBottom: 20,
    backgroundColor: '#fbeaff',
    color: 'white',
    padding: 8
  },
  addToDoTextInput: {
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#ccc',
    padding: 5,
    margin: 2,
    flex: 1
  },
  list: {
    color: 'black',
    margin: 2,
  },
  listItem: {
    flex:1,
    flexDirection: 'row',
    margin: 5
  },
  listItemText: {
    color: '#00c9a7',
    fontSize: 20
  },
  listItemDelete: {
    marginStart: 10,
    color: '#2c73d2',
    fontWeight: 'bold',
    fontSize: 20
  },
});

# Exercise 02 : Calculator / Expo

In this exercise you will learn the basics of the React Native development:

  - using and rendering build-in components
  - understand state and props
  - event handling
  - using styles

## Screenshots
### Addition
<img src='./addition.png' height=450>

### Subtraction
<img src='./subtraction.png' height=450>

### Division
<img src='./division.png' height=450>

### Multiplication
<img src='./multiplication.png' height=450>
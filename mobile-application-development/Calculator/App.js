import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, Text, TextInput, View, Button, TouchableWithoutFeedback, Keyboard, TouchableOpacity } from 'react-native';

export default function App() {
  // hooks
  const [number1, setNumber1] = useState('0');
  const [number2, setNumber2] = useState('0');
  const [result, setResult] = useState('0');

  function calcAddition() {
   
    const num1 = parseInt(number1, 10);
    const num2 = parseInt(number2, 10);

    setResult((num1 + num2).toString());
  }

  function calcMultiplication() {
   
    const num1 = parseInt(number1, 10);
    const num2 = parseInt(number2, 10);

    setResult((num1 * num2).toString());
  }

  function calcSubstraction() {
   
    const num1 = parseInt(number1, 10);
    const num2 = parseInt(number2, 10);

    setResult((num1 - num2).toString());
  }

  function calcDivision() {
   
    const num1 = parseInt(number1, 10);
    const num2 = parseInt(number2, 10);

    setResult((num1 / num2).toString());
  }

  // Get keyboard out of the way by touching screen outside keyboard
  const dismissKeyboard = () => {
    Keyboard.dismiss();
  };

  // custom button, for Button cannot be modified
  const customButton = (title, onPress) => (
    <TouchableOpacity style={styles.customButton} onPress={onPress}>
      <Text style={styles.buttonText}>{title}</Text>
    </TouchableOpacity>
  );


  return (
    <TouchableWithoutFeedback onPress={dismissKeyboard}>
    <View style={styles.container}>
      <Text style={styles.calculator}>Calculator</Text>
      <View style={styles.row}>
        <View style={styles.text}>
          <Text>Number 1:</Text>
        </View>
        <View style={styles.textInput}>
          <TextInput 
          value={number1}
          onChangeText={text => setNumber1(text)}
          placeholder='0' 
          style={{textAlign:'right'}} 
          keyboardType='numeric'
          ></TextInput>
        </View>
      </View>
      <View style={styles.row}>
        <View style={styles.text}>
          <Text>Number 2:</Text>
        </View>
        <View style={styles.textInput}>
          <TextInput 
          value={number2} 
          onChangeText={text => setNumber2(text)}
          placeholder='0' 
          style={{textAlign:'right'}} 
          keyboardType='numeric'
          ></TextInput>
        </View>
      </View>
      <View style={styles.buttonRow}>
        {/*  Adding own custom buttons */}
        {customButton(" + ", () => calcAddition())}
        {customButton(" - ", () => calcSubstraction())}
        {customButton(" * ", () => calcMultiplication())}
        {customButton(" / ", () => calcDivision())}
      </View>
      <View style={styles.row}>
        <View style={styles.text}>
          <Text>Result:</Text>
        </View>
        <View style={styles.textInput}>
          <TextInput 
          placeholder='0' 
          value={result}
          style={{textAlign:'right'}} 
          editable={false} >
          </TextInput>
        </View>
      </View>
      <StatusBar style="auto" />
    </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff', 
    alignItems: 'center',
    justifyContent: 'center',
  },
  calculator: {
    color: "#845EC2",
    fontSize: 50,
    fontWeight: 'bold',
    marginBottom: 20
  },
  row: {
    flexDirection: 'row',
    marginTop: 5
  },
  text: {
    backgroundColor: '#ff9671',
    justifyContent: 'center',
    padding: 5,
    width: 100
  },
  textInput: {
    color: '#845ec2',
    justifyContent: 'center',
    padding: 5,
    borderBottomWidth: 1.0,
    width: 100,
    marginLeft: 5
  },
  buttonRow: {
    flexDirection: 'row',
    marginTop: 20,
    marginBottom: 20,
    justifyContent: 'space-around',
    width: 220,
  },
  customButton: {        
    backgroundColor: '#845ec2',
    padding: 10,
    borderRadius: 5,
  },
  buttonText: {
    color: '#fff',
    fontSize: 15,   
  }

});

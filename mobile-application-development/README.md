# Mobile Application Development

## Course discription

In this course student will learn the basics of the mobile application developing with popular Cross-Platform Tools. The student can design, implement and publish mobile
applications to mobile devices. Course contains lectures,exercises and research assignment.

The student understands the prospects of different mobile technologies for implementation of functional applications in mobile devices. The student is able to implement small-scale applications for various mobile platforms using various mobile technologies. The student is able to work as a project group member in a project concerning a research assignment.

The topics discussed are commonly and currently used cross-platform mobile technologies that can be used to implement applications for various mobile platforms. The students deepen their competence by selecting one technology for a mobile device and implementing a research assignment on the topic area in small groups.

### Exercises

<a href='./ReactBasics/'>E01 Basics</a>

<a href='./Calculator/'>E02 Calculator</a>

<a href='./Todo/'>E03 Todo</a>

<a href='./Movies/'>E04 Movies</a>
